# Kicad Eskeleton

## Description
The KiCad Project Skeleton is a recommended structure for creating a project in KiCad, a popular electronic design automation (EDA) tool, to design printed circuit boards (PCBs). This project provides a clean and organized template to kickstart your PCB design process.

## Visuals
Visuals if needed

## Usage
What the user needs to know to use the pcb

## Support
c.araya.j19@gmail.com

## Road map
This section describes the features and status of the current version of the project

### Future implementations:
  * Future implementation 1
  * Future implementation 2

## Authors and acknowledgment
  * Carlos Araya Jiménez (c.araya.j19@gmail.com)

## License
This project is licensed under GPLv3+ (GNU General Public License version 3 or later).

## Project status
Active (2023.september.11)
